import { randomElement } from './random';


export interface Speech {
    speak(phrase: string, context: object, voice: string): void;
    speakRandomPhrase(phrases: string[], context: object, voice:string): void;
}


export class BrowserSpeech implements Speech {

    speak(phrase: string, context: object, voice: string) {
        const utter = new SpeechSynthesisUtterance(phrase);
        const v = window.speechSynthesis.getVoices().filter(voice_entry => voice_entry.name === voice)[0]
        utter.voice = v
        speechSynthesis.speak(utter);
    }

    speakRandomPhrase(phrases: string[], context: object, voice: string) {
        const phrase = randomElement(phrases);
        this.speak(phrase, context, voice);
    }
}


export class MockSpeech {
    transcript: string[] = [];

    reset() {
        this.transcript = [];
    }

    speak(phrase: string, context: object) {
        // TODO render template
        this.transcript.push(phrase);
    }

    speakRandomPhrase(phrases: string[], context: object) {
        if (phrases.length === 0) {
            return;
        }

        const phrase = randomElement(phrases);
        this.speak(phrase, context);
    }
}


export default function getSpeech(env: string = process.env.NODE_ENV!): Speech {
    if (env === 'test') {
        return new MockSpeech();
    } else {
        return new BrowserSpeech();
    }
}
